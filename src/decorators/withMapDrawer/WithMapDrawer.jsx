import React, { useEffect } from 'react';
import { connect } from 'react-redux';

const connector = connect(
  state => ({
    gMaps: state.gMaps,
    mapData: state.mapData,
    mapObject: state.mapObject,
    pointsOfInterest: state.pointsOfInterest,
    markers: state.markers,
    rootMarker: state.rootMarker,
  }),
  dispatch => ({
    dispatchMarker(marker) {
      dispatch({ type: 'ADD_MARKER', marker });
    },
    dispatchClear() {
      dispatch({ type: 'RECIVED_POINTS_OF_INTEREST', result: null });
      dispatch({ type: 'CLEAR_MARKERS' });
    },
  }),
);

const withMapDrawer = WrappedComponent => connector(({
  gMaps, mapObject, mapData, pointsOfInterest, markers, rootMarker, dispatchMarker, dispatchClear,
}) => {
  useEffect(() => {
    if (gMaps && mapObject && mapData === null) {
      markers.forEach((marker) => {
        marker.setMap(null);
      });
      dispatchClear();
    }
  }, [gMaps, mapObject, mapData, markers, dispatchClear]);

  useEffect(() => {
    if (gMaps && mapObject && mapData && mapData.places) {
      const position = {
        lat: mapData.places[0].latitude,
        lng: mapData.places[0].longitude,
      };
      mapObject.setCenter(position);

      const zoomSettings = {
        '1 mile': 16,
        '3 miles': 14,
        '5 miles': 14,
        '10 miles': 13,
        '15 miles': 13,
        [null]: 12,
      };

      mapObject.setZoom(zoomSettings[mapData.searchFilter.selectedRadius[0]]);

      const marker = new gMaps.Marker({ position, map: mapObject });
      marker.root = true;

      dispatchMarker(marker);
    }
  }, [gMaps, mapObject, mapData, dispatchMarker]);

  useEffect(() => {
    if (gMaps && mapObject && pointsOfInterest) {
      pointsOfInterest.forEach((place) => {
        const marker = new gMaps.Marker({
          position: place.geometry.location,
          map: mapObject,
          title: place.name,
          icon: {
            url: `https://maps.gstatic.com/mapfiles/place_api/icons/${place.type}-71.png`,
            scaledSize: new gMaps.Size(32, 32),
          },
        });

        marker.addListener('click', () => {
          const request = {
            origin: rootMarker.position,
            destination: marker.position,
            travelMode: 'DRIVING',
          };

          gMaps.directionsService.route(request, (result, status) => {
            if (status === 'OK') {
              gMaps.directionsDisplay.setDirections(result);
            }
          });
        });

        dispatchMarker(marker);
      });
    }
  }, [gMaps, mapObject, pointsOfInterest, rootMarker, dispatchMarker]);

  return <WrappedComponent />;
});

export default withMapDrawer;
