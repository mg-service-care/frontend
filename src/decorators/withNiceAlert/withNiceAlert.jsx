import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import NiceAlert from '../../components/NiceAlert';

const connector = connect(
  state => ({
    errorMessage: state.errorMessage,
  }),
  dispatch => ({
    dispatchNiceAlertDismiss: () => dispatch({ type: 'NICE_ALERT_DISMISS' }),
  }),
);

const withNiceAlert = WrappedComponent => connector(({
  errorMessage, dispatchNiceAlertDismiss, ...args
}) => (
  <Fragment>
    <WrappedComponent {...args} />
    {errorMessage && (
      <NiceAlert message={errorMessage} onClose={dispatchNiceAlertDismiss} />
    )}
  </Fragment>
));

export default withNiceAlert;
