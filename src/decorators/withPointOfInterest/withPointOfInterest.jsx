import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';

const connector = connect(
  state => ({
    gMaps: state.gMaps,
    mapData: state.mapData,
    mapObject: state.mapObject,
    pointsOfInterest: state.pointsOfInterest,
  }),
  dispatcher => ({
    dispatchResults(results) {
      dispatcher({ type: 'RECIVED_POINTS_OF_INTEREST', results });
    },
  }),
);

const withPointOfInterest = WrappedComponent => connector(({
  gMaps, mapData, mapObject, dispatchResults, pointsOfInterest,
}) => {
  useEffect(() => {
    const mapSettedUp = gMaps && mapData && mapData.places;

    if (mapSettedUp && !pointsOfInterest) {
      const service = new gMaps.places.PlacesService(mapObject);

      const location = {
        lat: mapData.places[0].latitude,
        lng: mapData.places[0].longitude,
      };

      const radiusMiles = mapData.searchFilter.selectedRadius.length
        ? mapData.searchFilter.selectedRadius[0]
        : 70;

      const radius = (parseInt(radiusMiles, 10) * 1609.344).toFixed(0);

      const possibleTypes = {
        'View all': ['gas_station', 'restaurant', 'lodging'],
        'Gas stations': ['gas_station'],
        Restaurants: ['restaurant'],
        Hotels: ['lodging'],
      };

      const typesMapped = mapData.searchFilter.selectedPois
        .map(item => possibleTypes[item])
        .reduce((acc, cur) => [...acc, ...cur], []);

      const types = typesMapped.length ? typesMapped : possibleTypes['View all'];

      const query = {
        location,
        radius,
        openNow: true,
      };

      const promises = Promise.all(types.map(type => new Promise((resolve, reject) => {
        const search = Object.assign({}, query, { type });

        service.search(search, (results, status, pagination) => {
          if (status !== 'OK') {
            return reject(new Error(status));
          }
          return resolve(results.map(item => Object.assign({}, item, { type })));
        });
      })));

      promises.then((results) => {
        dispatchResults(
          results
            .reduce((acc, cur) => [...acc, ...cur], [])
            .sort(() => Math.random() - 0.5),
        );
      });
    }
  }, [mapData, gMaps, mapObject, dispatchResults, pointsOfInterest]);

  return (
    <Fragment>
      <WrappedComponent />
    </Fragment>
  );
});

export default withPointOfInterest;
