/* eslint-disable no-underscore-dangle */
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import fetch from 'cross-fetch';
import es6Promise from 'es6-promise';

es6Promise.polyfill();

const connector = connect(
  state => ({
    searchFilter: state.searchFilter,
  }),
  dispatch => ({
    dispatchDatabaseSearchResult(data) {
      dispatch({ type: 'MAP_DATABASE_SEARCH_RESULT', data });
    },
    dispatchErrorOnFetching(data) {
      dispatch({ type: 'DISPATCH_ERROR_ON_FETCHING', data });
    },
  }),
);

const withSearchMotor = WrappedComponent => connector(({
  searchFilter, dispatchDatabaseSearchResult, dispatchErrorOnFetching, ...args
}) => {
  useEffect(() => {
    if (!searchFilter.licensePlate) {
      return;
    }

    dispatchDatabaseSearchResult(null);

    fetch(`${process.env.REACT_APP_API_URL}/vehicles/search/findByLicensePlate?license_plate=${encodeURIComponent(searchFilter.licensePlate)}`)
      .then(res => res.json(), dispatchErrorOnFetching.bind(new Error('Error fetching data')))
      .then((data) => {
        if (!data && !data.page && !data.page.totalElements) {
          throw new Error('Error fetching data');
        }
        if (data.page.totalElements === 0) {
          throw new Error('License plate not found');
        }
        if (data.page.totalElements > 1) {
          throw new Error('More than one license plate founded');
        }
        if (data.page.totalElements === 1) {
          return data._embedded.vehicles[0];
        }
        throw new Error('Error fetching data');
      }, dispatchErrorOnFetching)
      .then(vehicle => ({ vehicle, placesUrl: vehicle._links.places.href }))
      .then(({ vehicle, placesUrl }) => {
        fetch(placesUrl)
          .then(res => res.json(), dispatchErrorOnFetching.bind(new Error('Error fetching data')))
          .then((data) => {
            if (!data && !data._embedded) {
              throw new Error('Error fetching data');
            }
            if (data._embedded.length < 1) {
              throw new Error('No localization place founded for this license plate');
            }
            return data._embedded.places;
          }, dispatchErrorOnFetching)
          .then((places) => {
            dispatchDatabaseSearchResult({ vehicle, places, searchFilter });
          })
          .catch((err) => {
            dispatchErrorOnFetching(err);
          });
      })
      .catch((err) => {
        dispatchErrorOnFetching(err);
      });
  }, [searchFilter, dispatchDatabaseSearchResult, dispatchErrorOnFetching]);

  return <WrappedComponent {...args} />;
});

export default withSearchMotor;
