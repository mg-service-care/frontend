import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Grid.module.css';

const Grid = ({ component: Component, className, ...rest }) => (
  <Component className={classNames(styles.grid, className)} {...rest} />
);

Grid.propTypes = {
  component: PropTypes.elementType,
  className: PropTypes.string,
};

Grid.defaultProps = {
  component: 'div',
  className: '',
};

export default Grid;
