import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Grid.module.css';

const Col = ({
  component: Component, col, className, ...rest
}) => (
  <Component className={classNames(styles[`col-${col}`], styles.col, className)} {...rest} />
);

Col.propTypes = {
  component: PropTypes.elementType,
  col: PropTypes.number,
  className: PropTypes.string,
};

Col.defaultProps = {
  component: 'div',
  col: 12,
  className: '',
};

export default Col;
