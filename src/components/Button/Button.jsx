import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.css';

const Button = ({ text, onClick }) => (
  <div>
    <button type="button" className={styles.btn} onClick={onClick}>
      {text}
    </button>
  </div>
);

Button.defaultProps = {
  text: 'Some text',
  onClick: () => {},
};

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
};

export default Button;
