/* eslint-env jest, mocha */
import React from 'react';
import renderer from 'react-test-renderer';
import Button from './Button.jsx';

describe('Button', () => {
  it('renders correctly', () => {
    const component = renderer.create(
      <Button value="testing text"/>
    );
    expect(component.toJSON()).toMatchSnapshot();
  });
});

