/* globals google */
import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styles from './Map.module.css';

const injectScript = (dispatchMapLoaded) => {
  const key = process.env.REACT_APP_GOOGLE_MAPS_API_KEY;
  const head = document.querySelector('head');

  const script = document.createElement('script');
  script.src = `https://maps.googleapis.com/maps/api/js?key=${key}&libraries=places`;
  script.addEventListener('load', dispatchMapLoaded, { once: true, passive: true });
  if (process.env.REACT_APP_INITIALIZE_MAPS) {
    head.appendChild(script);
  }
};

const connector = connect(
  state => ({
    mapLoaded: state.mapLoaded,
  }),
  dispatch => ({
    dispatchMapLoaded() {
      dispatch({ type: 'MAP_LOADED' });
    },
    dispatchMapObject(object) {
      dispatch({ type: 'MAP_OBJECT', object });
    },
  }),
);

const Map = ({ mapLoaded, dispatchMapLoaded, dispatchMapObject }) => {
  useEffect(() => {
    if (!mapLoaded) {
      injectScript(dispatchMapLoaded);
    } else {
      const directionsService = new google.maps.DirectionsService();
      const directionsDisplay = new google.maps.DirectionsRenderer();

      const root = document.querySelector(`.${styles.mainMap}`);
      const mapObject = new google.maps.Map(
        root,
        {
          center: {
            lat: parseFloat(process.env.REACT_APP_MAPS_INITIAL_LATITUDE),
            lng: parseFloat(process.env.REACT_APP_MAPS_INITIAL_LONGITUDE),
          },
          zoom: parseInt(process.env.REACT_APP_MAPS_INITIAL_ZOOM, 10),
          disableDefaultUI: true,
        },
      );

      directionsDisplay.setMap(mapObject);

      google.maps.directionsDisplay = directionsDisplay;
      google.maps.directionsService = directionsService;

      dispatchMapObject({ mapObject, gMaps: google.maps });
    }
  }, [mapLoaded, dispatchMapLoaded, dispatchMapObject]);

  return (
    <Fragment>
      <div className={styles.mainMap} />
    </Fragment>
  );
};

Map.propTypes = {
  mapLoaded: PropTypes.bool,
  dispatchMapLoaded: PropTypes.func,
  dispatchMapObject: PropTypes.func,
};

Map.defaultProps = {
  mapLoaded: false,
  dispatchMapLoaded: () => {},
  dispatchMapObject: () => {},
};

export default connector(Map);
