import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import styles from './Select.module.css';
import { ReactComponent as Arrow } from './arrow.svg';
import Options from './Options';

const Select = ({ onChange = () => {}, open = false, placeholder = "Pick one", options = [], selected: initialSelected = [], multiple = false }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [timeoutId, setTimeoutId] = useState(null);
  const [selected, setSelected] = useState(initialSelected);

  useEffect(() => {
    setSelected(initialSelected);
  }, [initialSelected]);

  const handleSetValue = ({ value }) => {
    if (multiple) {
      setSelected((current) => {
        if (!~current.indexOf(value)) {
          const result = [...current, value];
          onChange(result);
          return result;
        }

        const result = current.filter((item) => item !== value);
        onChange(result);
        return result;
      });
    } else {
      onChange([value]);
      setSelected([value]);
    }
  };

  return (
    <div
      onFocus={() => clearTimeout(timeoutId)}
      onBlur={() => setTimeoutId(setTimeout(() => { setIsOpen(false); }))}
      className={styles.root}
    >
      <button
        className={classnames(styles.input, { [styles.selected]: selected.length })}
        onClick={() => setIsOpen(!isOpen)}
      >
        <div className={styles.inputText}>{selected.join(', ') || placeholder}</div>
        <Arrow className={styles.arrow} />
      </button>

      {(open || isOpen) && <Options options={options} selected={selected} onSetValue={handleSetValue} />}
    </div>
  );
};

export default Select;
