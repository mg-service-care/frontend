import React, { useState, useCallback, useEffect } from 'react';
import classnames from 'classnames';
import styles from './Select.module.css';
import util from 'util';

const normalize = (item) => {
  if (util.isString(item)) {
    return { label: item, value: item, selected: false };
  }

  if (!util.isObject(item) || (!item.label && !item.value)) {
    return false;
  }

  if (!!item.label ^ !!item.value) {
    const value = item.label || item.value;
    return { label: value, value: value, selected: false };
  }

  if (item.label && item.value) {
    return { label: item.label, value: item.value, selected: false };
  }
};

const markSelectedItems = (selectedItems) => (item) => {
  if (~selectedItems.indexOf(item.value)) {
    return Object.assign(item, { selected: true });
  }

  return item;
};

const Options = ({ options, selected = [], onSetValue = () => {} }) => {
  const [items, setItems] = useState([]);

  useEffect(() => {
    setItems(options.map(normalize)
        .filter((e) => e)
        .map(markSelectedItems(selected)));

  }, [setItems, options, selected]);

  const handleSelect = useCallback((event, item) => {
    onSetValue(item);
  }, [onSetValue]);

  return (
    <div className={styles.options}>
      {items.map((item, index) => (
        <div
          key={index}
          className={styles.optionItem}
        >
          <button
            onClick={(event) => handleSelect(event, item)}
            className={classnames({ [styles.optionItemSelected]: item.selected })}
          >
            {item.label}
          </button>
        </div>
      ))}
    </div>
  );
};

export default Options;
