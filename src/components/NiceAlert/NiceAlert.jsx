import React, { Fragment, useCallback } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import styles from './NiceAlert.module.css';

const NiceAlert = ({ title, message, onClose = () => {} }) => {
  const checkExit = useCallback((event) => {
    const isEscape = event.key === 'Escape';

    if (!isEscape) {
      return;
    }

    onClose();
  }, [onClose]);

  return (
    <Fragment>
      <div role="none" className={styles.backdrop} onClick={onClose} onKeyPress={checkExit} />
      <div className={styles.wrapper}>
        <div className={styles.niceAlert}>
          <div className={styles.title}>
            <div className={styles.titleContent}>{title}</div>
            {/* eslint-disable-next-line react/no-danger */}
            <button type="button" className={styles.close} dangerouslySetInnerHTML={{ __html: '&times;' }} onClick={onClose} />
          </div>
          <div className={styles.content}>
            {message}
          </div>
          <div className={styles.action}>
            <Button text="Close" className={styles.button} onClick={onClose} />
          </div>
        </div>
      </div>
    </Fragment>
  );
};

NiceAlert.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string.isRequired,
  onClose: PropTypes.func,
};

NiceAlert.defaultProps = {
  title: '',
  onClose: () => {},
};

export default NiceAlert;
