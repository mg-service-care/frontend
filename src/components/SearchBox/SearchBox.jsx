import React, { Fragment, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styles from './SearchBox.module.css';
import { Grid, Col } from '../Grid';
import Button from '../Button';
import Select from '../Select';

const connector = connect(
  state => ({}),
  dispatch => ({
    dispatchSearchData(data) {
      dispatch({ type: 'SEARCH_DATA', data });
    },
  }),
);

const SearchBox = ({ dispatchSearchData }) => {
  const poiValues = [
    'View all',
    'Gas stations',
    'Restaurants',
    'Hotels',
  ];

  const radiusValues = [
    '1 mile',
    '3 miles',
    '5 miles',
    '10 miles',
    '15 miles',
  ];

  const [selectedPois, setSelectedPois] = useState([]);
  const [selectedRadius, setSelectedRadius] = useState([]);
  const [licensePlate, setLicensePlate] = useState('');

  const handleChangePois = useCallback((value) => {
    if (~value.indexOf(poiValues[0])) {
      if (value.length > 1 && value[0] === poiValues[0]) {
        return setSelectedPois(value.splice(1));
      }
      return setSelectedPois([poiValues[0]]);
    }
    setSelectedPois(value);
  }, [setSelectedPois, poiValues]);

  const handleChangeRadius = useCallback((value) => {
    setSelectedRadius(value);
  }, [setSelectedRadius]);

  const handleChangeLicensePlate = useCallback((event) => {
    setLicensePlate(event.target.value);
  }, []);

  const handleSearch = useCallback(() => {
    const data = {
      licensePlate,
      selectedPois,
      selectedRadius,
    };

    dispatchSearchData(data);
  }, [dispatchSearchData, licensePlate, selectedPois, selectedRadius]);

  const handleKeyPress = useCallback((event) => {
    if (event.key === 'Enter') {
      handleSearch();
    }
  }, [handleSearch]);

  return (
    <Fragment>
      <Grid className={styles.searchBox}>
        <Col col={5}>
          <input className={styles.search} value={licensePlate} placeholder="Search by license plate" onChange={handleChangeLicensePlate} onKeyPress={handleKeyPress} />
        </Col>
        <Col col={3}>
          <Select className={styles.control} placeholder="Select POI type" multiple options={poiValues} selected={selectedPois} onChange={handleChangePois} />
        </Col>
        <Col col={3}>
          <Select className={styles.control} placeholder="Select radius" selected={selectedRadius} onChange={handleChangeRadius} options={radiusValues} />
        </Col>
        <Col col={2}>
          <Button text="Apply" onClick={() => handleSearch()} />
        </Col>
      </Grid>
    </Fragment>
  );
};

SearchBox.propTypes = {
  dispatchSearchData: PropTypes.func.isRequired,
};

export default connector(SearchBox);
