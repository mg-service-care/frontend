import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

const store = createStore(
  (state, action) => {
    if (action.type === 'MAP_LOADED') {
      return Object.assign({}, state, { mapLoaded: true });
    }

    if (action.type === 'MAP_OBJECT') {
      return Object.assign(
        {},
        state,
        {
          mapObject: action.object.mapObject,
          gMaps: action.object.gMaps,
        },
      );
    }

    if (action.type === 'MAP_DATABASE_SEARCH_RESULT') {
      return Object.assign({}, state, { mapData: action.data });
    }

    if (action.type === 'SEARCH_DATA') {
      return Object.assign({}, state, { searchFilter: action.data });
    }

    if (action.type === 'RECIVED_POINTS_OF_INTEREST') {
      return Object.assign({}, state, { pointsOfInterest: action.results });
    }

    if (action.type === 'CLEAR_MARKERS') {
      return Object.assign({}, state, { markers: [], rootMarker: null });
    }

    if (action.type === 'DISPATCH_ERROR_ON_FETCHING') {
      return Object.assign({}, state, { mapData: {}, errorMessage: action.data.message });
    }

    if (action.type === 'NICE_ALERT_DISMISS') {
      return Object.assign({}, state, { errorMessage: null });
    }

    if (action.type === 'ADD_MARKER') {
      state.markers.push(action.marker);
      if (action.marker.root) {
        return Object.assign({}, state, { rootMarker: action.marker });
      }
      return state;
    }

    return {
      mapLoaded: false,
      gMaps: null,
      mapObject: null,
      mapData: null,
      searchFilter: {},
      errorMessage: null,
      markers: [],
      rootMarker: null,
      pointsOfInterest: null,
    };
  },
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
