import React from 'react';
import compose from 'compose-function';
import './App.css';
import Map from './components/Map';
import SearchBox from './components/SearchBox';
import {
  withSearchMotor, withNiceAlert, withMapDrawer, withPointOfInterest
} from './decorators';

const App = () => (
  <div className="App">
    <SearchBox />
    <Map />
  </div>
);

const composition = compose(
  withPointOfInterest,
  withMapDrawer,
  withNiceAlert,
  withSearchMotor,
);

export default composition(App);
