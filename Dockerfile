FROM node:10-alpine as build

WORKDIR /app
COPY ./package.json ./package-lock.json ./
RUN npm install --no-optional
COPY ./ ./
RUN npm run build

FROM node:10-alpine

RUN mkdir -p /app
RUN npm i -g serve
COPY --from=build /app/build/ /app

CMD ["serve", "-s", "app", "--listen", "tcp://0.0.0.0:3000"]